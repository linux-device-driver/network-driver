#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/mutex.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <net/rtnetlink.h>
#include <net/netns/generic.h>
#include <linux/rtnetlink.h>
#include <uapi/linux/if_arp.h>
#include <linux/etherdevice.h>
#include <linux/rwsem.h>
#include <net/dst.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("VIJAY M RAJ <mvr250697@gmail.com>");

#define DEBUG(fmt)	printk(KERN_DEBUG "snull:"fmt"\n")

#define PDEBUG(fmt)	printk(KERN_DEBUG fmt":%s\n",__func__)

static void snull_setup(struct net_device *dev);

/* --------------------------------- Network namespace --------------------------------*/
struct snull_device {
	struct list_head list;
	struct mutex lock;
};

static unsigned int net_id;

struct net_device *device=NULL;

static __net_init int snull_net_init(struct net *net) {
	
	int ret;	
	struct snull_device *net_dev=net_generic(net,net_id);

	#ifndef CHECK
	PDEBUG("Begin");
	#endif
	
	if(device)
		goto RET;

	device=alloc_netdev(0,"snull%d",NET_NAME_UNKNOWN,snull_setup);
	if(!device)
		return -ENOMEM;

	dev_net_set(device,net);
	ret=register_netdev(device);
	if(ret) {
		free_netdev(device);
		return -EINVAL;
	}

	#ifndef CHECK
	PDEBUG("End");
	#endif
RET:
	return 0;
	 
}

static __net_exit void snull_net_exit(struct net *net) {
	#ifndef CHECK
	PDEBUG("Begin");
	#endif
	
	if(device) {
		unregister_netdev(device);
		device=NULL;
	}

	#ifndef CHECK
	PDEBUG("End");
	#endif
}

static struct pernet_operations __net_initdata snull_net_ops = {
	.init	=snull_net_init,
	.exit	=snull_net_exit,
	.id	=&net_id,
	.size	=sizeof(struct snull_device)
};

/*-------------------------------------------------------------------------------------*/

struct net_device *dev;


static int snull_init(struct net_device *dev) {

	#ifndef CHECK
	PDEBUG("Begin");
	#endif

	dev->tstats=netdev_alloc_pcpu_stats(struct pcpu_sw_netstats);
	if(!dev->tstats)
		return -ENOMEM;

	#ifndef CHECK
	PDEBUG("End");
	#endif
	return 0;
}

static void snull_uninit(struct net_device *dev) {

	#ifndef CHECK
	PDEBUG("Begin");
	#endif

	free_percpu(dev->tstats);

	#ifndef CHECK
	PDEBUG("End");
	#endif
}


static netdev_tx_t snull_xmit (struct sk_buff *skb, 
				struct net_device *dev) {
	#ifndef CHECK
	PDEBUG("Begin");
	#endif

	skb_tx_timestamp(skb);
	skb_orphan(skb);

	skb_dst_force(skb);

	skb->protocol=eth_type_trans(skb,dev);
	if(netif_receive_skb(skb) == NET_RX_SUCCESS) {
		DEBUG("Packet received successfully");
	}
	dev_lstats_add(dev,skb->len);

	#ifndef CHECK
	PDEBUG("End");
	#endif

	return NETDEV_TX_OK;
}

static int snull_change_carrier(struct net_device *dev, bool new_carrier) {

	#ifndef CHECK
	PDEBUG("Begin");
	#endif

	if(new_carrier)
		netif_carrier_on(dev);
	else
		netif_carrier_off(dev);

	#ifndef CHECK
	PDEBUG("End");
	#endif

	return 0;
}

static int snull_open(struct net_device *dev) {
	#ifndef CHECK
	PDEBUG("Begin");
	#endif

	netif_start_queue(dev);

	#ifndef CHECK
	PDEBUG("End");
	#endif

	return 0;
}

static int snull_stop(struct net_device *dev) {
	#ifndef CHECK
	PDEBUG("Begin");
	#endif

	netif_stop_queue(dev);
	#ifndef CHECK
	PDEBUG("End");
	#endif

	return 0;
}


static void snull_get_stats64(struct net_device *dev,
			struct rtnl_link_stats64 *stats) {
	u64 pkt, bytes;
	dev_lstats_read(dev,&pkt,&bytes);

	stats->rx_packets	= pkt;
	stats->tx_packets	= pkt;
	stats->rx_bytes		= bytes;
	stats->tx_bytes		= bytes;

}

static const struct ethtool_ops eth_ops= {
	.get_ts_info	=ethtool_op_get_ts_info
};

static const struct net_device_ops ops ={
	.ndo_open		= snull_open,
	.ndo_stop		= snull_stop,
	.ndo_init		= snull_init,
	.ndo_uninit		= snull_uninit,
	.ndo_start_xmit		= snull_xmit,
	.ndo_validate_addr	= eth_validate_addr,
	.ndo_set_mac_address	= eth_mac_addr,
	.ndo_change_carrier	= snull_change_carrier,
	.ndo_get_stats64	= snull_get_stats64
};

static void snull_setup(struct net_device *dev) {
	#ifndef CHECK
	PDEBUG("Begin");
	#endif

	ether_setup(dev);

	dev->netdev_ops		=&ops;
	dev->ethtool_ops	=&eth_ops;
	dev->needs_free_netdev	=true;
	dev->hard_header_len	=ETH_HLEN;
	dev->min_header_len	=ETH_HLEN;
	dev->addr_len		=ETH_ALEN;
	dev->type               =ARPHRD_ETHER;
	dev->flags		|= IFF_UP | IFF_RUNNING;
	dev->priv_flags		|= IFF_LIVE_ADDR_CHANGE | IFF_NO_QUEUE;
	dev->features		|= NETIF_F_SG | NETIF_F_FRAGLIST;
	dev->features		|= NETIF_F_HW_CSUM;
	dev->features		|= NETIF_F_ALL_TSO;
	dev->featues		|= NETIF_F_RXCSUM;
	dev->hw_features	|= dev->features;


	eth_hw_addr_random(dev);

	dev->min_mtu=0;
	dev->max_mtu=0;


	#ifndef CHECK
	PDEBUG("End");
	#endif
}

static int __init snull_dev_init(void)  {

	int ret;

	#ifndef CHECK
	PDEBUG("Begin");
	#endif

	ret=register_pernet_device(&snull_net_ops);
	if(ret)
		goto OUT1;


	#ifndef CHECK
	PDEBUG("End");
	#endif

	return 0;

OUT:
	return ret;

OUT1:
	return ret;

}

static void __exit snull_dev_exit(void) {
	
	#ifndef CHECK
	PDEBUG("Begin");
	#endif

	unregister_pernet_device(&snull_net_ops);

	#ifndef CHECK
	PDEBUG("End");
	#endif
}

module_init(snull_dev_init);
module_exit(snull_dev_exit);
