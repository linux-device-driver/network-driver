
INSTALL_DIR := modules
obj-m := network_dr.o

KERNELDIR ?= /lib/modules/$(shell uname -r)/build

all default: modules
install: modules_install

modules modules_install help:
	$(MAKE) -C $(KERNELDIR) M=$(shell pwd) $@
	@mkdir -v $(INSTALL_DIR)
	@mv *.ko *.order *.mod.c *mod *.symvers *.o $(INSTALL_DIR)

clean:
	rm -rf $(INSTALL_DIR)



